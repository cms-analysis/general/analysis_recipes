#!/usr/bin/env bash

# NOTE: This script assumes that pocketcoffea is installed in the caller's
#       environment. This is the case on GitLab where we use the Docker image.

# Define the associative array for string mapping
declare -A eras_map=(
    ["2016preVFP_UL"]="2016_PreVFP"
    ["2016postVFP_UL"]="2016_PostVFP"
    ["2017_UL"]="2017"
    ["2018_UL"]="2018"
    ["2022_Summer22"]="2022_preEE"
    ["2022_Summer22EE"]="2022_postEE"
    ["2023_Summer23"]="2023_preBPix"
    ["2023_Summer23BPix"]="2023_postBPix"
)

action() {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"

    local recipe="$1"
    local era="$2"
    local input="$3"
    local output="$4"

    # Make sure the configs have been cloned.
    source "${this_dir}/setup.sh"

    local sample=TT_DL
    local era_internal="${eras_map[$era]}"
    local recipe_folder="${this_dir}/configs/configs/tests/corrections/${recipe}"

    set -e

    python "${this_dir}/prepare_dataset.py" \
        --sample ${sample} \
        --year ${era_internal} \
        --output "${recipe_folder}/datasets.json" \
        --file "${input}"

    cd "${recipe_folder}"
    pocket-coffea run \
        --cfg config.py \
        -o output \
        --filter-years ${era_internal} \
        -ro custom_run_options.yaml

    python "${this_dir}/get_validation_output.py" \
        --input output/output_all.coffea \
        --output "${output}" \
        --sample ${sample} \
        --year ${era_internal}
}
action "$@"
