#!/usr/bin/env bash

action() {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"

    #
    # PocketCoffea setup
    #

    if [ ! -d "${this_dir}/configs" ]; then
        git clone https://github.com/PocketCoffea/AnalysisConfigs.git "${this_dir}/configs" --depth 1
    fi

}
action "$@"
