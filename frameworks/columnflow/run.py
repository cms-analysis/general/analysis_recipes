# coding: utf-8

from __future__ import annotations

import os
import sys
import importlib

# Maps correctionlib names to columnflow names
ERAS = {
    "2016preVFP_UL": "16pre",
    "2016postVFP_UL": "16post",
    "2017_UL": "17",
    "2018_UL": "18",
    "2022_Summer22": "22pre",
    "2022_Summer22EE": "22post",
    "2023_Summer23": "23pre",
    "2023_Summer23BPix": "23post",
}


def main() -> int:
    from argparse import ArgumentParser

    # custom types
    abspath = lambda s: os.path.abspath(os.path.expandvars(os.path.expanduser(s)))

    # setup the parser
    parser = ArgumentParser(
        description="dispatcher for columnflow analysis recipes",
    )
    parser.add_argument(
        "--era",
        choices=ERAS.keys(),
        help="name of the era to run in",
    )
    parser.add_argument(
        "--recipe",
        help="name of the recipe to run",
    )
    parser.add_argument(
        "--input",
        type=abspath,
        help="path to the input file to read",
    )
    parser.add_argument(
        "--output",
        type=abspath,
        help="path to the output file to write",
    )
    args = parser.parse_args()

    # find the recipe module
    try:
        mod = importlib.import_module(f"cf_analysis.recipe_scripts.{args.recipe}")
    except ImportError:
        print(f"recipe module {args.recipe} not found", file=sys.stderr)
        return 1
    except Exception as e:
        print(f"recipe module {args.recipe} failed to load: {e}", file=sys.stderr)
        return 2

    # extract the recipe function
    try:
        func = getattr(mod, f"recipe_{args.recipe}")
    except Exception as e:
        print(f"recipe function recipe_{args.recipe} not found: {e}", file=sys.stderr)
        return 3

    # run the recipe
    try:
        func(
            era_name=ERAS[args.era],
            input_file=args.input,
            output_file=args.output,
        )
        return 0
    except Exception as e:
        e.args = (f"recipe {args.recipe} failed: {e}",)
        raise


if __name__ == "__main__":
    sys.exit(main())
