# columnflow analysis recipes

- `setup.sh`: sets up a columnflow based analysis from scratch and copies `files` into it that contain the recipes to be tested.
- `recipe_scripts`: contains the recipes to be tested, including some minimal configuration files.
- `run.py`: dispatcher script called by the CI that fowards all calls to the appropriate recipe script.
