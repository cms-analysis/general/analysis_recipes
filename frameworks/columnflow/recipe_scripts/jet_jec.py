# coding: utf-8

from __future__ import annotations

import law
from columnflow.calibration.cms.jets import jec
from columnflow.util import DotDict
from cf_analysis.recipe_scripts.util import create_analysis_objects, iterate_input

CAMPAIGNS = {
    "16pre": "Summer19UL16APV",
    "16post": "Summer19UL16",
    "17": "Summer19UL17",
    "18": "Summer19UL18",
    "22pre": "Summer22_22Sep2023",
    "22post": "Summer22EE_22Sep2023",
    "23pre": "Summer23Prompt23",
    "23post": "Summer23BPixPrompt23",
}
VERSIONS = {
    2016: "V7",
    2017: "V5",
    2018: "V5",
    2022: "V2",
    2023: "V1",
}


def recipe_jet_jec(
    era_name: str,
    input_file: str,
    output_file: str,
) -> None:

    # create fake analysis objects
    # (this would normally be part of the analysis setup)
    analysis, campaign, config, shift, era = create_analysis_objects(era_name)
    print(era)

    # add minimal entries to comply with the jec calibrator
    # (this would normally be part of the config setup)
    config.x.external_files = DotDict.wrap({
        "jet_jerc": f"/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/JME/{era.json_pog_era}/jet_jerc.json.gz",  # noqa
    })
    config.x.jec = DotDict.wrap({
        "Jet": {
            "campaign": CAMPAIGNS[era_name],
            "version": VERSIONS[era.year],
            "jet_type": "AK4PFchs" if era.year < 2019 else "AK4PFPuppi",
            "levels": ["L1L2L3Res"],
            "levels_for_type1_met": ["L1FastJet"],
            "uncertainty_sources": [],
        },
    })

    # instantiate and setup the calibrator
    # (this would normally be done automatically in columnflow's task structure)
    calibrator = jec(inst_dict={
        "task": None,
        "analysis_inst": analysis,
        "config_inst": config,
        # Duck-type a dataset with the bare minimum required
        "dataset_inst": DotDict.wrap({"is_mc": True, "is_data": False}),
        "local_shift_inst": shift,
        "global_shift_inst": shift,
    })
    calibrator.run_setup(
        # mimic the external file bundle structure
        reqs=DotDict.wrap({
            "jec": {
                "external_files": {
                    "files": {
                        "jet_jerc": law.LocalFileTarget(config.x.external_files.jet_jerc),
                    },
                },
            },
        }),
        # not needed
        inputs={},
    )

    with open(output_file, "wt", encoding="utf-8") as stream:
        # iterate through input
        for events, _ in iterate_input(input_file, calibrator.used_columns):
            events = calibrator(events)

            # write
            for ev in events:
                jets_data = ", ".join(
                    f"{pt:.6f},{mass:.6f}" for pt, mass in zip(ev.Jet.pt, ev.Jet.mass)
                )
                stream.write(f"{ev.event},{ev.run},{ev.luminosityBlock}, {jets_data}\n")
