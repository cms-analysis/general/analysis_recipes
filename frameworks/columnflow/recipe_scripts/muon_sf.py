# coding: utf-8

from __future__ import annotations


def recipe_muon_sf(
    era_name: str,
    input_file: str,
    output_file: str,
) -> None:
    import law
    from columnflow.production.cms.muon import muon_weights
    from columnflow.util import DotDict
    from cf_analysis.recipe_scripts.util import (
        OutputType, create_analysis_objects, iterate_input, write_output,
    )

    # create fake analysis objects
    # (this would normally be part of the analysis setup)
    analysis, campaign, config, shift, era = create_analysis_objects(era_name)
    print(era)

    # add minimal entries to comply with the muon weights producer
    # (this would normally be part of the config setup)
    config.x.external_files = DotDict.wrap({
        "muon_sf": f"/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/MUO/{era.json_pog_era}/muon_Z.json.gz",  # noqa
    })
    config.x.muon_sf_names = (
        "NUM_TightID_DEN_TrackerMuons",
        era.json_pog_era,
    )

    # instantiate and setup the weight producer
    # (this would normally be done automatically in columnflow's task structure)
    producer = muon_weights(inst_dict={
        "task": None,
        "analysis_inst": analysis,
        "config_inst": config,
        "local_shift_inst": shift,
        "global_shift_inst": shift,
    })
    producer.run_setup(
        # mimic the external file bundle structure
        reqs=DotDict.wrap({
            "muon_weights": {
                "external_files": {
                    "files": {
                        "muon_sf": law.LocalFileTarget(config.x.external_files.muon_sf),
                    },
                },
            },
        }),
        # not needed
        inputs={},
    )

    # simply list to hold results
    outputs: OutputType = []

    # iterate through input
    for events, chunk_pos in iterate_input(input_file, producer.used_columns.union({"Muon.phi", "Muon.tightId"})):
        events = producer(
            events,
            # note: it would be helpful if correction sets knew about range constraints of inputs
            muon_mask=((events.Muon.pt >= 15) & (abs(events.Muon.eta) <= 2.4) & events.Muon.tightId),
        )

        # save the sf product (i.e, the event weight)
        outputs += list(zip(
            events.event,
            events.run,
            events.luminosityBlock,
            events.muon_weight,
        ))

    # write outputs
    write_output(output_file, outputs)
