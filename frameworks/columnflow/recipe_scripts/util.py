# coding: utf-8

"""
Utilities commonly used across by recipe scripts.
"""

from __future__ import annotations

import os
import shutil
from dataclasses import dataclass
from typing import Union

import order as od


OutputType = tuple[Union[int, float], ...]


@dataclass
class Era(object):

    name: str
    run: int
    year: int
    json_pog_era: str
    egamma_year: str = ""  # set in post_init when actually empty

    def __post_init__(self) -> None:
        # dynamic defaults
        if not self.egamma_year:
            self.egamma_year = str(self.year)

    @classmethod
    def from_name(cls, name: str) -> Era:
        if name == "16pre":
            return cls(name=name, run=2, year=2016, json_pog_era="2016preVFP_UL", egamma_year="2016preVFP")
        if name == "16post":
            return cls(name=name, run=2, year=2016, json_pog_era="2016postVFP_UL", egamma_year="2016postVFP")
        if name == "17":
            return cls(name=name, run=2, year=2017, json_pog_era="2017_UL")
        if name == "18":
            return cls(name=name, run=2, year=2018, json_pog_era="2018_UL")
        if name == "22pre":
            return cls(name=name, run=3, year=2022, json_pog_era="2022_Summer22", egamma_year="2022Re-recoBCD")
        if name == "22post":
            return cls(name=name, run=3, year=2022, json_pog_era="2022_Summer22EE", egamma_year="2022Re-recoE+PromptFG")
        if name == "23pre":
            return cls(name=name, run=3, year=2023, json_pog_era="2023_Summer23", egamma_year="2023PromptC")
        if name == "23post":
            return cls(name=name, run=3, year=2023, json_pog_era="2023_Summer23BPix", egamma_year="2023PromptD")
        raise ValueError(f"unknown era {name}")


def create_analysis_objects(
    era_name: str,
) -> tuple[od.Analysis, od.Campaign, od.Config, od.Shift, Era]:
    analysis = od.Analysis(name="analysis_recipes", id=1)
    campaign = od.Campaign(name="campaign_recipes", id=1, aux={"era": era_name})
    config = analysis.add_config(campaign)
    shift = config.add_shift(name="nominal", id=0)
    era = Era.from_name(era_name)

    return analysis, campaign, config, shift, era


def expand_path(path: str) -> str:
    return os.path.abspath(os.path.expandvars(os.path.expanduser(path)))


def prepare_directory(path: str, clear: bool = False) -> str:
    path = expand_path(path)

    if os.path.exists(path):
        if os.path.isfile(path):
            raise IOError(f"file exists at {path}")
        if clear:
            shutil.rmtree(path)

    if not os.path.exists(path):
        os.makedirs(path)

    return path


def write_output(output_file: str, outputs: OutputType) -> None:
    output_file = expand_path(output_file)
    prepare_directory(os.path.dirname(output_file))
    with open(output_file, "w") as f:
        for row in outputs:
            f.write(f"{','.join(map(str, row))}\n")


def iterate_input(
    input_file: str,
    read_columns: set[str],
    source_type: str = "coffea_root",
    chunk_size: int = 10_000,
):
    from columnflow.columnar_util import Route, ChunkedIOHandler, mandatory_coffea_columns

    # iterate through file
    with ChunkedIOHandler(
        input_file,
        # open via coffea's NanoEvents factory
        source_type=source_type,
        # limit the columns to read
        read_columns=set(map(Route, mandatory_coffea_columns | read_columns)),
        chunk_size=chunk_size,
        debug=True,
    ) as io:
        for events, chunk_pos in io:
            yield events, chunk_pos
