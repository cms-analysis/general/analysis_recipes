# coding: utf-8

from __future__ import annotations


def recipe_electron_sf(
    era_name: str,
    input_file: str,
    output_file: str,
) -> None:
    import law
    from columnflow.production.cms.electron import electron_weights
    from columnflow.util import DotDict
    from cf_analysis.recipe_scripts.util import (
        OutputType, create_analysis_objects, iterate_input, write_output,
    )

    # create fake analysis objects
    # (this would normally be part of the analysis setup)
    analysis, campaign, config, shift, era = create_analysis_objects(era_name)
    print(era)

    # add minimal entries to comply with the electron weights producer
    # (this would normally be part of the config setup)
    config.x.external_files = DotDict.wrap({
        "electron_sf": f"/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/EGM/{era.json_pog_era}/electron.json.gz",  # noqa
    })
    config.x.electron_sf_names = (
        "UL-Electron-ID-SF" if era.run == 2 else "Electron-ID-SF",
        era.egamma_year,
        "wp80iso",
    )

    # instantiate and setup the weight producer
    # (this would normally be done automatically in columnflow's task structure)
    producer = electron_weights(inst_dict={
        "task": None,
        "analysis_inst": analysis,
        "config_inst": config,
        "local_shift_inst": shift,
        "global_shift_inst": shift,
    })
    producer.run_setup(
        # mimic the external file bundle structure
        reqs=DotDict.wrap({
            "electron_weights": {
                "external_files": {
                    "files": {
                        "electron_sf": law.LocalFileTarget(config.x.external_files.electron_sf),
                    },
                },
            },
        }),
        # not needed
        inputs={},
    )

    # simply list to hold results
    outputs: OutputType = []

    # iterate through input
    for events, chunk_pos in iterate_input(input_file, producer.used_columns):
        events = producer(
            events,
            # note: it would be helpful if correction sets knew about range constraints of inputs
            electron_mask=(events.Electron.pt >= 10),
        )

        # save the sf product (i.e, the event weight)
        outputs += list(zip(
            events.event,
            events.run,
            events.luminosityBlock,
            events.electron_weight,
        ))

    # write outputs
    write_output(output_file, outputs)
