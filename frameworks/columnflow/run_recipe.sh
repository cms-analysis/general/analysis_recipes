#!/usr/bin/env bash

action() {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"

    local recipe="$1"
    local era="$2"
    local input="$3"
    local output="$4"

    source "${this_dir}/setup.sh" ""
    cf_sandbox venv_columnar python "${this_dir}/run.py" \
            --recipe "${recipe}" \
            --era "${era}" \
            --input "${input}" \
            --output "${output}" \
            || return "$?"
}
action "$@"
