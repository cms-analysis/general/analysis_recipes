#!/usr/bin/env bash

action() {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"
    local ret

    #
    # columnflow setup
    #

    local cf_analysis="${this_dir}/cf_analysis"
    local cf_exists="$( [ -d "${cf_analysis}" ] && echo "true" || echo "false" )"

    if ! ${cf_exists}; then
        (
            cd "${this_dir}"
            # pipe decisions about project name etc into the interactive columnflow project setup script
            export CF_CREATE_ANALYSIS_VERBOSE="true"
            {
                echo cf_analysis
                echo cf_analysis
                echo CR
                echo cms_minimal
                echo False
            } | bash -c "$(curl -Ls https://raw.githubusercontent.com/columnflow/columnflow/master/create_analysis.sh)"
        )
        ret="$?"
        if [ "${ret}" != "0" ]; then
            >&2 echo "columnflow project setup failed with exit code ${ret}"
            return "${ret}"
        fi
    fi

    #
    # copy files
    #

    # copy files on gitlab ci, symlink files locally
    if [ ! -z "${GITLAB_CI}" ] && [ "${GITLAB_CI}" != "false" ]; then
        rm -rf "${cf_analysis}/cf_analysis/recipe_scripts"
        cp -r "${this_dir}/recipe_scripts" "${cf_analysis}/cf_analysis/"
    elif [ ! -d "cf_analysis/cf_analysis/recipe_scripts" ]; then
        ( cd "${cf_analysis}/cf_analysis" && ln -s "${this_dir}/recipe_scripts" "." )
    fi

    #
    # install
    #

    # overall setup
    source "${cf_analysis}/setup.sh" ""
    ret="$?"
    if [ "${ret}" != "0" ]; then
        >&2 echo "columnflow installation failed with exit code ${ret}"
        return "${ret}"
    fi
    ! ${cf_exists} && pip cache purge

    # trigger sandboxes initially
    if ! ${cf_exists}; then
        cf_sandbox venv_columnar pip cache purge
        ret="$?"
        if [ "${ret}" != "0" ]; then
            >&2 echo "columnflow sandbox setup failed with exit code ${ret}"
            return "${ret}"
        fi
    fi
}
action "$@"
