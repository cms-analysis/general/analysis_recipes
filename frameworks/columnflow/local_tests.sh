#!/usr/bin/env bash

action() {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"

    local skip_recipe="$( [ "$1" = "1" ] && echo "true" || echo "false" )"

    local era="17"
    local recipe="electron_sf"

    if ! ${skip_recipe}; then
        cf_sandbox venv_columnar python "${this_dir}/run.py" \
            --era "${era}" \
            --recipe "${recipe}" \
            --input "/eos/cms/store/group/cat/datasets/recipes/tt_dl_10k_${era}.root" \
            --output "${this_dir}/result_${era}_${recipe}.txt" \
            || return "$?"
    fi
    python "${this_dir}/../../scripts/validate.py" \
        --era "${era}" \
        --recipe "${recipe}" \
        --framework columnflow \
        --truth "/eos/cms/store/group/cat/datasets/recipes/tt_dl_10k_${era}_${recipe}_truth.txt" \
        --result "${this_dir}/result_${era}_${recipe}.txt"
}
action "$@"
