# CMSJMECalculators Analysis Recipes

This is the folder for [CMSJMECalculators](https://gitlab.cern.ch/cms-analysis/general/CMSJMECalculators)
analysis recipes. The install script `setup.sh` sets up a micromamba environment
in the `install` folder in the current directory. It can be activated by running:
```sh
source install/.bashrc
micromamba activate
```
