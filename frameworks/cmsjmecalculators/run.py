import sys
from argparse import ArgumentParser
from pathlib import Path

import ROOT

import CMSJMECalculators as calc
import CMSJMECalculators.config as calcConfig
from CMSJMECalculators import utils


CVMFS = Path("/cvmfs/cms.cern.ch/rsync/cms-nanoAOD/jsonpog-integration/POG/JME/")

def main() -> int:
    # setup the parser
    parser = ArgumentParser(
        description="dispatcher for columnflow analysis recipes",
    )
    parser.add_argument(
        "--era",
        help="name of the era to run in",
    )
    parser.add_argument(
        "--jet-type",
        default="AK4PFchs",
        help="type of jet to compute corrections for",
    )
    parser.add_argument(
        "--jerc-tag",
        default="Summer19UL17_V5_MC",
        help="JERC tag of the corrections to use",
    )
    parser.add_argument(
        "--input",
        help="path to the input file to read",
    )
    parser.add_argument(
        "--output",
        type=Path,
        help="path to the output file to write",
    )
    args = parser.parse_args()

    calc.loadJMESystematicsCalculators()

    config = calcConfig.JetVariations(str(CVMFS / args.era / "jet_jerc.json.gz"), args.jet_type)
    config.jecTag = args.jerc_tag
    config.jecLevel = "L1L2L3Res"

    input_file = ROOT.TFile.Open(args.input)  # pylint:disable=E1101
    tree = input_file["Events"]

    calculator = ROOT.JetVariationsCalculator(config.create())  # pylint:disable=E1101

    with args.output.open("wt", encoding="utf-8") as stream:
        for ev in tree:
            jet_vars = calculator.produce(*utils.getJetMETArgs(tree, isMC=True, forMET=False))
            jets = ", ".join(
                f"{pt:.6f},{mass:.6f}" for pt, mass in zip(jet_vars.pt(0), jet_vars.mass(0))
            )
            stream.write(f"{ev.event},{ev.run},{ev.luminosityBlock}, {jets}\n")


if __name__ == "__main__":
    sys.exit(main())
