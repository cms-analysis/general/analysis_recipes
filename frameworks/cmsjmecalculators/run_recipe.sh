#!/usr/bin/env bash

action() {
    local shell_is_zsh="$( [ -z "${ZSH_VERSION}" ] && echo "false" || echo "true" )"
    local this_file="$( ${shell_is_zsh} && echo "${(%):-%x}" || echo "${BASH_SOURCE[0]}" )"
    local this_dir="$( cd "$( dirname "${this_file}" )" && pwd )"

    local recipe="$1"
    local era="$2"
    local input="$3"
    local output="$4"

    set -e

    if [ "${recipe}" != "jet_jec" ]; then
        echo "jet_jec is the only supported recipe" >&2
        exit 1
    fi

    # Define which jets to use
    declare -A jet_type_map=(
        ["2016preVFP_UL"]="AK4PFchs"
        ["2016postVFP_UL"]="AK4PFchs"
        ["2017_UL"]="AK4PFchs"
        ["2018_UL"]="AK4PFchs"
        ["2022_Summer22"]="AK4PFPuppi"
        ["2022_Summer22EE"]="AK4PFPuppi"
        ["2023_Summer23"]="AK4PFPuppi"
        ["2023_Summer23BPix"]="AK4PFPuppi"
    )

    # Define which tag to use
    declare -A tag_map=(
        ["2016preVFP_UL"]="Summer19UL16APV_V7_MC"
        ["2016postVFP_UL"]="Summer19UL16_V7_MC"
        ["2017_UL"]="Summer19UL17_V5_MC"
        ["2018_UL"]="Summer19UL18_V5_MC"
        ["2022_Summer22"]="Summer22_22Sep2023_V2_MC"
        ["2022_Summer22EE"]="Summer22EE_22Sep2023_V2_MC"
        ["2023_Summer23"]="Summer23Prompt23_V1_MC"
        ["2023_Summer23BPix"]="Summer23BPixPrompt23_V1_MC"
    )

    source "${this_dir}/setup.sh"
    python "${this_dir}/run.py" \
            --jet-type "${jet_type_map[$era]}" \
            --jerc-tag "${tag_map[$era]}" \
            --era "${era}" \
            --input "${input}" \
            --output "${output}" \
            || return "$?"
}
action "$@"
