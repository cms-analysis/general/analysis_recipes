# CMS Analysis Recipes [![pipeline status](https://gitlab.cern.ch/cms-analysis/general/analysis_recipes/badges/master/pipeline.svg)](https://gitlab.cern.ch/cms-analysis/general/analysis_recipes/-/commits/master) [![documentation](https://img.shields.io/badge/docs-cms--analysis-success)](http://cms-analysis.docs.cern.ch/guidelines/snippets/corrections) ![python version](https://img.shields.io/badge/python-%E2%89%A53.9-blue)

Collection of CI pipelines and interfaces to supported frameworks that implement central analysis recipes (calculation of scale factors, corrections, etc.) whose outputs are tested against POG recommendations.

**For analysts:** Please checkout the documentation linked in the badge above.
The overview contains badges to the latest CI pipeline that is triggered twice per day (at 2am and 2pm).

**For framework developers:** See [developer infos below](#developer-infos).

---

- [Pipeline statuses](#pipeline-statuses)
  - [electron_sf](#electron_sf)
  - [jet_jec](#jet_jec)
  - [muon_sf](#muon_sf)
- [Developer infos](#developer-infos)
  - [Test files](#test-files)
  - [Truth files](#truth-files)
  - [Implementing framework recipes](#implementing-framework-recipes)
  - [CI pipelines](#ci-pipelines)
  - [GitLab shortcomings](#gitlab-shortcomings)

## Pipeline statuses

### `electron_sf`

Metric: product of electron SFs per event (default: 1.0)
- Correction set: `UL-Electron-ID-SF`
- Working point: `wp80iso`

<table>
<tr>
  <th>Era</th>
  <th>columnflow</th>
  <th>PocketCoffea</th>
</tr>
<tr>
  <td>2016preVFP_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Belectron_sf,%202016preVFP_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Belectron_sf,%202016preVFP_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2016postVFP_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Belectron_sf,%202016postVFP_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Belectron_sf,%202016postVFP_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2017_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Belectron_sf,%202017_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Belectron_sf,%202017_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2018_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Belectron_sf,%202018_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Belectron_sf,%202018_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2022_Summer22</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Belectron_sf,%202022_Summer22%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Belectron_sf,%202022_Summer22%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2022_Summer22EE</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Belectron_sf,%202022_Summer22EE%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Belectron_sf,%202022_Summer22EE%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2023_Summer23</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Belectron_sf,%202023_Summer23%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Belectron_sf,%202023_Summer23%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2023_Summer23BPix</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Belectron_sf,%202023_Summer23BPix%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Belectron_sf,%202023_Summer23BPix%5D" alt="Badge"/></td>
</tr>
</table>

### `jet_jec`

Metric: $p_\mathrm T$ and mass of all jets after applying the recommended JEC.

<table>
<tr>
  <th>Era</th>
  <th>CMSJMECalculators</th>
  <th>columnflow</th>
</tr>
<tr>
  <td>2016preVFP_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=cmsjmecalculators%3A%20%5Bjet_jec,%202016preVFP_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bjet_jec,%202016preVFP_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2016postVFP_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=cmsjmecalculators%3A%20%5Bjet_jec,%202016postVFP_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bjet_jec,%202016postVFP_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2017_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=cmsjmecalculators%3A%20%5Bjet_jec,%202017_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bjet_jec,%202017_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2018_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=cmsjmecalculators%3A%20%5Bjet_jec,%202018_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bjet_jec,%202018_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2022_Summer22</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=cmsjmecalculators%3A%20%5Bjet_jec,%202022_Summer22%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bjet_jec,%202022_Summer22%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2022_Summer22EE</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=cmsjmecalculators%3A%20%5Bjet_jec,%202022_Summer22EE%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bjet_jec,%202022_Summer22EE%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2023_Summer23</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=cmsjmecalculators%3A%20%5Bjet_jec,%202023_Summer23%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bjet_jec,%202023_Summer23%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2023_Summer23BPix</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=cmsjmecalculators%3A%20%5Bjet_jec,%202023_Summer23BPix%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bjet_jec,%202023_Summer23BPix%5D" alt="Badge"/></td>
</tr>
</table>

### `muon_sf`

Metric: product of muon SFs per event (default: 1.0), for all muons passing the Tight ID and with $|\eta|<2.4$, $p_\mathrm T>15$ GeV.
- Correction set: `NUM_TightID_DEN_TrackerMuons`
- Scale factor: `nominal` 

<table>
<tr>
  <th>Era</th>
  <th>columnflow</th>
  <th>PocketCoffea</th>
</tr>
<tr>
  <td>2016preVFP_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bmuon_sf,%202016preVFP_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Bmuon_sf,%202016preVFP_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2016postVFP_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bmuon_sf,%202016postVFP_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Bmuon_sf,%202016postVFP_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2017_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bmuon_sf,%202017_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Bmuon_sf,%202017_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2018_UL</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bmuon_sf,%202018_UL%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Bmuon_sf,%202018_UL%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2022_Summer22</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bmuon_sf,%202022_Summer22%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Bmuon_sf,%202022_Summer22%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2022_Summer22EE</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bmuon_sf,%202022_Summer22EE%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Bmuon_sf,%202022_Summer22EE%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2023_Summer23</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bmuon_sf,%202023_Summer23%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Bmuon_sf,%202023_Summer23%5D" alt="Badge"/></td>
</tr>
<tr>
  <td>2023_Summer23BPix</td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=columnflow%3A%20%5Bmuon_sf,%202023_Summer23BPix%5D" alt="Badge"/></td>
  <td><img src="https://gitlab.cern.ch/api/v4/projects/186488/jobs/artifacts/master/raw/badges/badge.svg?job=pocketcoffea%3A%20%5Bmuon_sf,%202023_Summer23BPix%5D" alt="Badge"/></td>
</tr>
</table>

## Developer infos

### Test files

- Located at `/eos/cms/store/group/cat/datasets/recipes/<ERA>/*.root`
- Per era, first file the respective ttbar (dilepton) MiniAOD dataset
- Processed with `scripts/make_nano.sh`

### Truth files

- Located at `/eos/cms/store/group/cat/datasets/recipes/<ERA>/<RECIPE>.txt`
- Text files contain one line per event
- See [implementation details below](#implementing-recipes) for content

### Implementing recipes

Each framework needs a script called `frameworks/<FRAMEWORK_DIRECTORY>/run_recipe.sh`. It takes the following parameters as positional arguments:
- The recipe name
- The era name (using the same convention as in the [JSON-POG repo](https://gitlab.cern.ch/cms-nanoAOD/jsonpog-integration))
- The input NanoAOD file
- The location of the output file

The script should take care of setting up the environment for the framework. It should return a non-zero output if running the framework fails.

The output file is a simple text file with one line per input event. It contains comma-separated values:
- The first three entries should refer to the event number, the run number, and the luminosity block.
- All subsequent numbers should contain the output of the specific recipe per object in question.

Here is a simple example:
```
24075003,1,24076,1.0
24075008,1,24076,0.97983503
24075005,1,24076,0.99055785
```

In case substantial installation is needed, a second script called `setup.sh` can be added to avoid repeating the setup too often. This is particularly helpful to avoid needlessly spending resources on the CI.

The CI setup for recipes is located in `.gitlab/ci/frameworks/<FRAMEWORK>.yml` (included from the main `.gitlab-ci.yml`). Typical recipes have one or two jobs:
- A first job to install the framework, if needed
- A second job to run the recipe

See existing frameworks for examples.

It is possible to run the recipe from within a container image, in which case the installation job is likely not needed. The image needs to contain `python3` and `wget`, or they can be installed on the fly by adding the appropriate command to the `EXTRA_SETUP` variable.

### CI pipelines

[CI pipelines](https://gitlab.cern.ch/cms-analysis/general/analysis_recipes/-/pipelines) are **not** triggered on every commit but follow a **schedule** that triggers them twice per day (2am and 2pm).

However, pipelines can be triggered manually through the [pipeline interface](https://gitlab.cern.ch/cms-analysis/general/analysis_recipes/-/pipelines/new) using pre-configured job variables that control the behavior and shape of the pipeline:

- `BUILD_FRAMEWORKS`: Whether to (re)build frameworks before testing recipes. Otherwise framework builds are taken from previously cached runs.
- `TEST_ERAS`: Which era(s) to test.
- `TEST_RECIPES`: Which recipe(s) to test.
- `TEST_FRAMEWORKS`: Which framework(s) to test. 'none' triggers all framework-independent preparation steps to run.

Scheduled pipelines are configured to rebuild all frameworks and test/validate all recipes for all eras.
