# coding: utf-8

from __future__ import annotations

import os
import sys
from typing import Union


VKey = tuple[int, int, int]
VValue = tuple[Union[int, float], ...]


def validate(
    era_name: str,
    recipe_name: str,
    framework_name: str,
    truth_file: str,
    result_file: str,
    test_file: str | None = None,
    float_precision: int = 5,
) -> bool:
    print(f"validating recipe '{recipe_name}' in era '{era_name}' for framework '{framework_name}'")
    print(f"test file   -> {test_file}")
    print(f"truth file  -> {truth_file}")
    print(f"result file -> {result_file}")

    # read both truth and result data
    truth = read_text_file(truth_file)
    result = read_text_file(result_file)

    # perform a sequence of validatons checks below and store potential issues
    errors = []

    # check if keys are identical
    truth_keys = set(truth.keys())
    result_keys = set(result.keys())
    if (tr_diff := truth_keys - result_keys):
        error = f"{len(tr_diff)} missing event key(s) in result data"
        error += ", first five:" if len(tr_diff) > 5 else ":"
        for key in list(tr_diff)[:5]:
            error += f"\n  {key}"
        errors.append(error)
    if (rt_diff := result_keys - truth_keys):
        error = f"{len(rt_diff)} unknown event key(s) in result data"
        error += ", first five:" if len(rt_diff) > 5 else ":"
        for key in list(rt_diff)[:5]:
            error += f"\n  {key}"
        errors.append(error)

    # per truth entry, check all result entries
    length_errors = []
    value_errors = []
    for key, truth_value in truth.items():
        if key not in result:
            continue

        # check same length
        result_value = result[key]
        if len(truth_value) != len(result_value):
            length_errors.append(key)
            continue

        # check same values
        if not all(compare_values(*v, float_precision) for v in zip(truth_value, result_value)):
            value_errors.append(key)
            continue

    # handle event errors
    if length_errors:
        error = f"{len(length_errors)} event(s) found with wrong number of output values"
        error += ", first five:" if len(length_errors) > 5 else ":"
        for key in length_errors[:5]:
            error += f"\n  {key}: truth={truth[key]}, result={result[key]}"
        errors.append(error)
    if value_errors:
        error = f"{len(value_errors)} event(s) found with wrong output values"
        error += ", first five:" if len(value_errors) > 5 else ":"
        for key in value_errors[:5]:
            error += f"\n  {key}: truth={truth[key]}, result={result[key]}"
        errors.append(error)

    # handle overall errors
    if errors:
        print(f"detected {len(errors)} possible error source(s):")
        for error in errors:
            print(f"\n{error}", file=sys.stderr)
        return False

    # all checks passed
    return True


def expand_path(path: str) -> str:
    return os.path.abspath(os.path.expandvars(os.path.expanduser(path)))


def read_text_file(path: str) -> dict[VKey, VValue]:
    # read the file into a lookup table mapping (event, run, lumi) to the remaining values
    content = {}
    with open(expand_path(path), "r") as f:
        for line in f.readlines():
            try:
                event, run, lumi, *values = line.replace(" ", "").split(",")
            except:
                raise Exception(f"invalid line format: {line}")
            if not values or not values[0].strip():
                content[(int(event), int(run), int(lumi))] = tuple()
            else:
                content[(int(event), int(run), int(lumi))] = tuple(map(cast_number, values))
    return content


def cast_number(s: str) -> float | int:
    try:
        return int(s)
    except ValueError:
        return float(s)


def compare_values(a: int | float, b: int | float, precision: int) -> bool:
    # types must match
    if type(a) is not type(b):
        return False

    # for integers or when the precision is negative, values just have to be identical
    if isinstance(a, int) or precision < 0:
        return a == b

    # symmetric relative difference
    return 2 * abs(a - b) <= abs(a + b) * 10**-precision


def main() -> int:
    from argparse import ArgumentParser

    # custom types
    abspath = lambda s: os.path.abspath(os.path.expandvars(os.path.expanduser(s)))

    # setup the parser
    parser = ArgumentParser(
        description="tool for validating framework outputs against references",
    )
    parser.add_argument(
        "--era",
        help="name of the era to run in",
    )
    parser.add_argument(
        "--recipe",
        help="name of the recipe to run",
    )
    parser.add_argument(
        "--framework",
        help="name of the framework to validate",
    )
    parser.add_argument(
        "--truth",
        type=abspath,
        help="path to the truth file",
    )
    parser.add_argument(
        "--result",
        type=abspath,
        help="path to the result file",
    )
    parser.add_argument(
        "--test",
        type=abspath,
        default=None,
        help="path to the initial test input file",
    )
    args = parser.parse_args()

    # start the valudation
    try:
        success = validate(
            era_name=args.era,
            recipe_name=args.recipe,
            framework_name=args.framework,
            truth_file=args.truth,
            result_file=args.result,
            test_file=args.test,
        )
        return 0 if success else 1
    except Exception as e:
        print(f"validation failed: {e}", file=sys.stderr)
        return 2


if __name__ == "__main__":
    sys.exit(main())
