#!/usr/bin/env bash

set -e

usage() {
  (echo "Usage: $0 <era> [events]"
   echo "Where era is one of:"
   echo "  2016preVFP_UL"
   echo "  2016postVFP_UL"
   echo "  2017_UL"
   echo "  2018_UL"
   echo "  2022_Summer22"
   echo "  2022_Summer22EE"
   echo "  2023_Summer23"
   echo "  2023_Summer23BPix"
  ) >&2
  exit 1
}

if (( $# == 1 )); then
  era=$1
  events=10000
elif (( $# == 2 )); then
  era=$1
  events=$2
else
  usage
fi

prefix=TTTo2L2Nu

case $era in
  2016preVFP_UL)
    cmssw_release=CMSSW_10_6_32
    scram_arch=slc7_amd64_gcc700
    image=/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/el7:x86_64
    input_file=/store/mc/RunIISummer20UL16MiniAODAPVv2/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/MINIAODSIM/106X_mcRun2_asymptotic_preVFP_v11-v1/100000/01F4A6F7-4A2D-CF48-94EC-7C586DD313E3.root
    cmsDriver_args="--conditions 106X_mcRun2_asymptotic_preVFP_v11 --era Run2_2016_HIPM,run2_nanoAOD_106Xv2"
    ;;
  2016postVFP_UL)
    cmssw_release=CMSSW_10_6_32
    scram_arch=slc7_amd64_gcc700
    image=/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/el7:x86_64
    input_file=/store/mc/RunIISummer20UL16MiniAODv2/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/MINIAODSIM/106X_mcRun2_asymptotic_v17-v1/120000/038D9F5E-C340-FD45-B613-DD495181C237.root
    cmsDriver_args="--conditions 106X_mcRun2_asymptotic_v17 --era Run2_2016,run2_nanoAOD_106Xv2"
    ;;
  2017_UL)
    cmssw_release=CMSSW_10_6_32
    scram_arch=slc7_amd64_gcc700
    image=/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/el7:x86_64
    #input_file=/store/mc/RunIISummer20UL17MiniAODv2/TT_TuneCH3_13TeV-powheg-herwig7/MINIAODSIM/106X_mc2017_realistic_v9-v1/2540000/34FACD68-E06A-8943-BA51-C0B9F8464808.root
    input_file=/store/mc/RunIISummer20UL17MiniAODv2/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/MINIAODSIM/106X_mc2017_realistic_v9-v1/00000/0004BE39-823E-4A4B-9727-C2544050C4C0.root
    cmsDriver_args="--conditions 106X_mc2017_realistic_v9 --era Run2_2017,run2_nanoAOD_106Xv2"
    ;;
  2018_UL)
    cmssw_release=CMSSW_10_6_32
    scram_arch=slc7_amd64_gcc700
    image=/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/el7:x86_64
    input_file=/store/mc/RunIISummer20UL18MiniAODv2/TTTo2L2Nu_TuneCP5_13TeV-powheg-pythia8/MINIAODSIM/106X_upgrade2018_realistic_v16_L1v1-v1/00000/04A0B676-D63A-6D41-B47F-F4CF8CBE7DB8.root
    cmsDriver_args="--conditions 106X_upgrade2018_realistic_v16_L1v1 --era Run2_2018,run2_nanoAOD_106Xv2"
    ;;
  2022_Summer22)
    cmssw_release=CMSSW_13_0_13
    scram_arch=el8_amd64_gcc11
    image=/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/el8:x86_64
    input_file=/store/mc/Run3Summer22MiniAODv4/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/MINIAODSIM/130X_mcRun3_2022_realistic_v5-v2/2520000/a038d927-8ffd-4508-982c-44e8ab1fa02a.root
    cmsDriver_args="--conditions 130X_mcRun3_2022_realistic_v5 --scenario pp --era Run3"
    ;;
  2022_Summer22EE)
    cmssw_release=CMSSW_13_0_13
    scram_arch=el8_amd64_gcc11
    image=/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/el8:x86_64
    input_file=/store/mc/Run3Summer22EEMiniAODv4/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/MINIAODSIM/130X_mcRun3_2022_realistic_postEE_v6-v2/2520000/07720695-b82e-415a-b838-9c3c087724ab.root
    cmsDriver_args="--conditions 130X_mcRun3_2022_realistic_postEE_v6 --scenario pp --era Run3"
    ;;
  2023_Summer23)
    cmssw_release=CMSSW_13_0_13
    scram_arch=el8_amd64_gcc11
    image=/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/el8:x86_64
    input_file=/store/mc/Run3Summer23MiniAODv4/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/MINIAODSIM/130X_mcRun3_2023_realistic_v14-v2/70000/002f0920-e438-4d0d-b479-0d5071d13b84.root
    cmsDriver_args="--conditions 130X_mcRun3_2023_realistic_v14 --scenario pp --era Run3_2023"
    ;;
  2023_Summer23BPix)
    cmssw_release=CMSSW_13_0_13
    scram_arch=el8_amd64_gcc11
    image=/cvmfs/unpacked.cern.ch/registry.hub.docker.com/cmssw/el8:x86_64
    input_file=/store/mc/Run3Summer23BPixMiniAODv4/TTto2L2Nu_TuneCP5_13p6TeV_powheg-pythia8/MINIAODSIM/130X_mcRun3_2023_realistic_postBPix_v2-v3/2550000/06da80a1-1faa-4efc-a5b0-d74d2e2255e5.root
    cmsDriver_args="--conditions 130X_mcRun3_2023_realistic_postBPix_v2 --scenario pp --era Run3_2023"
    ;;
  *)
    usage
    echo "Unknown era $era" >&2
    exit 1
esac

# cc7 doesn't support modern SSL ciphers. For now this affects only some sites,
# but the situation will only worsen. Therefore we copy the (whole) file on the
# host OS, run locally, then delete it.
tmp=$(mktemp -d)
echo "Copying input MiniAOD..." >&2
xrdcp root://cms-xrd-global.cern.ch/$input_file $tmp/mini.root

# Enter output folder
mkdir -p $era
cd $era

# Useful file names
output=${prefix}_$(($events / 1000))k
output_mini=${output}_mini
output_nano=${output}_nano

# Script to execute in singularity
echo >$output.sh \
"#!/bin/bash

set -e

source /cvmfs/cms.cern.ch/cmsset_default.sh

# Get CMSSW
cd /cvmfs/cms.cern.ch/$scram_arch/cms/cmssw/$cmssw_release; cmsenv; cd -

echo 'Running edmCopyPickMerge...' >&2
edmCopyPickMerge inputFiles=file:$tmp/mini.root outputFile=$output_mini.root maxEvents=$events &>$output_mini.log || (
  echo 'Failed! Log below:'
  cat $output_mini.log
  rm -r $tmp  # cleanup not to fill /tmp
  exit 1
) >&2

rm -r $tmp  # cleanup not to fill /tmp

# edmCopyPickMerge changes the output file name, move it to the location we want
mv ${output_mini}_numEvent$events.root $output_mini.root

echo 'Running cmsDriver.py...' >&2
cmsDriver.py nano$era --filein file:$output_mini.root --fileout $output_nano.root --mc \
                      --eventcontent NANOAODSIM --datatier NANOAODSIM --step NANO \
                      --nThreads 8 -n -1 \
                      $cmsDriver_args &>$output_nano.log || (
  echo 'Failed! Log below:'
  cat $output_mini.log
  exit 1
) >&2
"
chmod +x $output.sh

export APPTAINER_BINDPATH=/afs,/cvmfs,/cvmfs/grid.cern.ch/etc/grid-security:/etc/grid-security,/cvmfs/grid.cern.ch/etc/grid-security/vomses:/etc/vomses,/etc/pki/ca-trust,/run/user,/tmp,/var/run/user,/etc/sysconfig,/etc:/orig/etc
home=$(readlink -f .)
singularity run --home $home:$home $image $home/$output.sh 

